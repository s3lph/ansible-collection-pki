#!/usr/bin/python3

ANSIBLE_METADATA = {
    'metadata_version': '0.1',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
---
module: crl

short_description: Create and sign a Certificate Revocation List

version_added: "2.9"

description:
    - "Create and sign a Certificate Revocation List"

options:
    path:
        description:
            - "CRL output path"
        required: true
    revocations:
        description:
            - "List of certificate files to revoke.  Expired certificates are not included."
        required: true
    ca_crt:
        description:
            - "Path to the CA's certificate"
        required: true
    ca_key:
        description:
            - "Path to the CA's private key"
        required: true
    passphrase:
        description:
            - "Passphrase of the CA's private key"
        required: false

author:
    - s3lph (@s3lph)
'''

EXAMPLES = '''
# Create a CRL
- name: Update CRL
  crl:
    path: /etc/ssl/ca.crl
    revocations:
      - /etc/ssl/foo.crt
      - /etc/ssl/bar.crt
      - serial=DEADBEEF
    ca_crt: /etc/ssl/certs/ca.crt
    ca_key: /etc/ssl/private/ca.key
    passphrase: "{{ ca passphrase }}"
'''

RETURN = '''
serials:
    description: List of revoked serial numbers
    type: list
    returned: always
path:
    description: CRL output filename
    type: str
    returned: always
'''

import os
import stat
import shutil

from ansible.module_utils.basic import AnsibleModule

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, serialization
from datetime import datetime, timedelta


def create_crl(*args, **kwargs):
    now = datetime.utcnow()
    then = now + timedelta(days=3650)

    with open(kwargs['ca_crt'], 'rb') as f:
        ca_crt = x509.load_pem_x509_certificate(f.read(), default_backend())
    passphrase = kwargs['passphrase'].encode() if kwargs.get('passphrase') else None
    with open(kwargs['ca_key'], 'rb') as f:
        ca_key = serialization.load_pem_private_key(f.read(), passphrase, default_backend())
    builder = x509.CertificateRevocationListBuilder()
    builder = builder \
        .issuer_name(ca_crt.subject) \
        .last_update(now) \
        .next_update(then)

    serials = set()
    for cf in kwargs['revocations']:
        if cf.startswith('serial='):
            serial = int(cf[len('serial='):].strip(), 16)
        else:
            with open(cf, 'rb') as f:
                crt = x509.load_pem_x509_certificate(f.read(), default_backend())
            if crt.not_valid_after < now:
                # Certificate is expired, don't include in the CRL
                continue
            serial = crt.serial_number
        serials.add(serial)

    rc_builder = x509.RevokedCertificateBuilder()
    for serial in serials:
        revoked = rc_builder \
            .serial_number(serial) \
            .revocation_date(now) \
            .build()
        builder = builder.add_revoked_certificate(revoked)

    crl = builder.sign(private_key=ca_key, algorithm=hashes.SHA256(), backend=default_backend())
    pem = crl.public_bytes(serialization.Encoding.PEM)
    with open(kwargs['path'], 'wb') as f:
        f.write(pem)

    return list(serials)


def run_module():

    module_args = dict(
        path = dict(type='str', required=True),
        revocations = dict(type='list', required=True),
        ca_crt = dict(type='str', required=True),
        ca_key = dict(type='str', required=True),
        passphrase = dict(type='str', required=False, default=None, no_log=True),
    )
    module = AnsibleModule(
        argument_spec = module_args,
        supports_check_mode = True
    )
    result = dict(
        changed = True,
        serials = [],
        path = module.params['path'],
    )
    
    if module.check_mode:
        module.exit_json(**result)

    result['serials'] = create_crl(**(module.params))    
    module.exit_json(**result)

def main():
    run_module()

if __name__ == '__main__':
    main()
